<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Price;
use App\Models\Image;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'name',
        'description',
        'materials',
        'bar_code',
        'visible',
        'subcategory_id',
        'brand_id',
        'price_id'
    ];

    public function actual_price(){
        return $this->hasOne(Price::class)->where('actual_price',true);
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public function image_portrait(){
        return $this->hasOne(Image::class)->where('order',1);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
