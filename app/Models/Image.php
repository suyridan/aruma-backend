<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Product;

class Image extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id',
        'order',
        'url'
    ];

    public function products()
    {
        return $this->belongsTo(Product::class);
    }
}
