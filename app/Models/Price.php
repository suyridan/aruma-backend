<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Price extends Model
{
    protected $fillable = [
        'product_id',
        'unit_price_purchase',
        'unit_price_recomended',
        'unit_price_sale',
        'quantity',
        'requested',
        'actual_price'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
