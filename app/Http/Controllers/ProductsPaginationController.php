<?php

namespace App\Http\Controllers;

use App\Models\Product;

class ProductsPaginationController extends Controller
{
    public function index($request = null){
        $products_eloq = Product::with([
            'prices:id,product_id,unit_price_sale,unit_price_purchase,quantity,requested',
            'image_portrait:id,product_id,url',
            'subcategory:id,category_id,name',
            'subcategory.category:id,name',
            'brand:id,name'
        ])
        ->where('visible', true)
        ->orderBy('id');

        if($request != null){
            $columns = ['name','code'];
            $words_search = explode(" ",$request);
            $products_eloq
            ->where(function ($query) use ($columns,$words_search) {
                foreach ($words_search as $word) {
                    $query = $query->where(function ($query) use ($columns,$word) {
                    foreach ($columns as $column) {
                        $query->orWhere($column,'ILIKE',"%$word%");
                    }
                    });
                }
            });
        }
        return $products_eloq->paginate(20);
    }
}


