<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class GetDataWebController extends Controller
{
    public function robando(){
        $ids_productos = Product::select('code','id')
            // ->whereBetween('id',[99,200])
            // ->whereDate('updated_at', '=', Carbon::today()->toDateString())
            // ->where('updated_at','=','created_at')
            // ->where('code', 849)
            // ->whereNull('materials')
            // ->whereNotIn('id', [730])
            // ->whereNull('description')
            ->doesnthave('images')
            ->where('id',1)
            ->get();

        return $ids_productos;

        // return $ids_productos;


        $data = [];
        $edit_producto = [];
        foreach ($ids_productos as $k => $producto) {
            DB::beginTransaction();
            $json = file_get_contents('https://bestdream.store/WS/details_sku?sku='.$producto->code);

            $data[$k] = json_decode($json)->feed_todos[0];

            if($data[$k]){
                $edit_producto[$k] = Product::find($producto->id);

                if($data[$k]->json){
                    if($data[$k]->json->descripcion){
                        $edit_producto[$k]->description = $data[$k]->json->descripcion;
                    }

                    if($data[$k]->json->bar_code){
                        $edit_producto[$k]->bar_code = $data[$k]->json->bar_code;
                    }

                    if($data[$k]->json->materiales){
                        $edit_producto[$k]->materials = $data[$k]->json->materiales;
                    }

                    if($data[$k]->json->imagen){
                        $edit_producto[$k]->images()->create([
                            'url'   => $data[$k]->json->imagen,
                            'order' => 1
                            ]);
                    }
                    else{
                        foreach ($data[$k]->images->feed as $e => $image) {
                            $edit_producto[$k]->images()->create([
                                'url'   => $image->imagen,
                                'order' => $e + 1
                            ]);
                        }
                    }
                }

                $edit_producto[$k]->save();
                DB::commit();
            }

        }


        return response()->json([
            'nombre_productos' => $edit_producto
        ]);
    }

    public function creando_documentos(){
        $data = [];
        $crear_producto = [];
        $crear_precio = [];
        for ($i=1; $i < 5000; $i++) { //max 3307

            $existe = Product::where('code',$i)->first();

            if(!$existe){
                $json = file_get_contents('https://bestdream.store/WS/details_sku?sku='.$i);

                $data[$i] = json_decode($json)->feed_todos[0];

                if($data[$i]){

                    if($data[$i]->json){
                        $crear_producto[$i] = Product::create([
                            'code'        => $data[$i]->json->id,
                            'name'        => $data[$i]->json->nombre,
                            'description' => $data[$i]->json->descripcion,
                            'materials'   => $data[$i]->json->materiales,
                            'bar_code'    => is_numeric($data[$i]->json->bar_code) ? $data[$i]->json->bar_code : null ,
                            'visible'     => true
                        ])
                            ->images()
                            ->create([
                                'url'   => $data[$i]->json->imagen,
                                'order' => 1
                            ]);

                        $crear_precio[$i] = Product::find($crear_producto[$i]->id)->prices()->create([
                            'unit_price_purchase'   => $data[$i]->json->precio_mayoreo,
                            'unit_price_recomended' => $data[$i]->json->precio_publico,
                            'unit_price_sale'       => $data[$i]->json->precio_filial,
                            'quantity'              => 0,
                            'requested'             => 0,
                            'actual_price'          => true
                        ]);
                    }
                }
            }

        }
        return response()->json([
            'crear_producto' => $crear_producto,
            'crear_precio'   => $crear_precio
        ]);

    }

    public function trayendo_marcas(){
        $data = [];
        $marcas = [];
        for ($i=3000; $i < 3400; $i++) { //max 3307

            $json = file_get_contents('https://bestdream.store/WS/details_sku?sku='.$i);

            $data[$i] = json_decode($json)->feed_todos[0];

            if($data[$i]){

                if($data[$i]->json){

                    if( !in_array( $data[$i]->json->marca, $marcas ) ){
                        array_push( $marcas,$data[$i]->json->marca );
                    }

                }
            }
        }
        return response()->json([
            'marcas' => $marcas
        ]);
    }

    public function update_marcas()
    {
        $productos = Product::select('id','code')->whereNull('brand_id')->get();
        $marcas = Brand::all();

        foreach ($productos as $producto) {
            $json = file_get_contents('https://bestdream.store/WS/details_sku?sku='.$producto->code);

            $data = json_decode($json)->feed_todos[0];

            if($data){

                if($data->json){

                    foreach ($marcas as $marca) {
                        if( strtoupper($marca->name) == strtoupper(trim($data->json->marca)) ){
                            $mod_producto = Product::find($producto->id);
                            $mod_producto->brand_id = $marca->id;
                            $mod_producto->save();
                        }
                    }

                }
            }
        }
    }
}
