<?php

namespace App\Http\Controllers;

use App\Models\Product;
use PDF;
use Illuminate\Http\Request;

class CatalogoController extends Controller
{
    public function index()
    {
        // $productos = Product::find([1,2,3,4,5,6,7,8,9,10]);
        // $productos = Product::all();

        $productos = Product::where('visible', true)
        ->with([
            'actual_price:id,product_id,unit_price_purchase',
            'images:id,product_id,url',
            'image_portrait:id,product_id,url',
            'subcategory:id,category_id,name',
            'subcategory.category:id,name'])
        ->where('name','!=','')
        // ->whereIn('id',[1,2,3,4,5,6,7,8,10])
        ->orderBy('id')
        ->get();

        // return $productos;

        view()->share('productos',$productos);
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('pdf.catalogo', $productos);

        // download PDF file with download method
        return $pdf->stream('pdf_file.pdf');

        return view('pdf.catalogo')
        ->with('productos', $productos );
    }
}
