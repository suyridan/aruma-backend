<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::with([
            'prices:id,product_id,unit_price_sale,unit_price_purchase,quantity,requested',
            'images:id,product_id,url',
            'subcategory:id,category_id,name',
            'subcategory.category:id,name'
        ])
        ->orderBy('id')
        ->paginate(20);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Product::create([
            'code'           => $request->input('code'),
            'name'           => $request->input('name'),
            'description'    => $request->input('description'),
            'visible'        => $request->input('visible'),
            'subcategory_id' => $request->input('subcategory_id'),
            'brand_id'       => $request->input('brand_id')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->code           = $request->input('code');
        $product->name           = $request->input('name');
        $product->description    = $request->input('description');
        $product->visible        = $request->input('visible');
        $product->subcategory_id = $request->input('subcategory_id');
        $product->brand_id       = $request->input('brand_id');
        return $product->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        return $product->delete();
    }
}
