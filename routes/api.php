<?php

use App\Http\Controllers\api\RegisterController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductsPaginationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('product',ProductController::class);
Route::get('/products_pagination/{filters?}', [ ProductsPaginationController::class, 'index' ]);


Route::post('register', [
    RegisterController::class,
    'register'
]);
Route::post('login', [
    RegisterController::class,
    'login'
]);
