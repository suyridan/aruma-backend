<?php

use App\Http\Controllers\CatalogoController;
use App\Http\Controllers\api\GetDataWebController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/generate_pdf', [ CatalogoController::class,'index']);

Route::get( '/robando', [ GetDataWebController::class, 'robando' ]);
Route::get( '/creando', [ GetDataWebController::class, 'creando_documentos' ]);
Route::get( '/trayendo_marcas', [ GetDataWebController::class, 'trayendo_marcas' ]);
Route::get( '/update_marcas', [ GetDataWebController::class ,'update_marcas' ]);
