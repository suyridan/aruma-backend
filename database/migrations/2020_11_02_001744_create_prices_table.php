<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id');
            $table->double('unit_price_purchase')->default(0);
            $table->double('unit_price_recomended')->default(0);
            $table->double('unit_price_sale')->default(0);
            $table->integer('quantity')->default(0);
            $table->integer('requested')->default(0);
            $table->boolean('actual_price')->default(true);
            $table->timestampsTz();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
