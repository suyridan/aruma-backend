<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class table_brands_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create(['id' => 1,'name' => 'BEAUTY CREATIONS']);
        Brand::create(['id' => 2,'name' => 'Bissu']);
        Brand::create(['id' => 3,'name' => 'Body Fantasies']);
        Brand::create(['id' => 4,'name' => 'DOCOLOR']);
        Brand::create(['id' => 5,'name' => 'GUGU']);
        Brand::create(['id' => 6,'name' => 'KARA']);
        Brand::create(['id' => 7,'name' => 'KJ']);
        Brand::create(['id' => 8,'name' => 'KLEAN COLOR']);
        Brand::create(['id' => 9,'name' => 'LA Colors']);
        Brand::create(['id' => 10,'name' => 'LA Girl']);
        Brand::create(['id' => 11,'name' => 'LOLI']);
        Brand::create(['id' => 12,'name' => 'MC']);
        Brand::create(['id' => 13,'name' => 'PerPlex']);
        Brand::create(['id' => 14,'name' => 'Pink Up']);
        Brand::create(['id' => 15,'name' => 'Profusion']);
        Brand::create(['id' => 16,'name' => 'PROSA']);
        Brand::create(['id' => 17,'name' => 'Rude']);
        Brand::create(['id' => 18,'name' => 'Trend Beauty']);

    }
}
