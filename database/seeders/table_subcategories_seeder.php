<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subcategory;

class table_subcategories_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subcategory::create(['id' => 1, 'category_id' => 1, 'name' => 'BROCHAS' ]);
        Subcategory::create(['id' => 2, 'category_id' => 1, 'name' => 'ESMALTES' ]);
        Subcategory::create(['id' => 3, 'category_id' => 1, 'name' => 'KIT OCACIÓN' ]);
        Subcategory::create(['id' => 4, 'category_id' => 1, 'name' => 'LABIOS' ]);
        Subcategory::create(['id' => 5, 'category_id' => 1, 'name' => 'OJOS' ]);
        Subcategory::create(['id' => 6, 'category_id' => 1, 'name' => 'ROSTRO' ]);
        Subcategory::create(['id' => 7, 'category_id' => 1, 'name' => 'ROSTRO Y UÑAS' ]);
    }
}
