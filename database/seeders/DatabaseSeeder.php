<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(table_brands_seeder::class);
        $this->call(table_categories_seeder::class);
        $this->call(table_subcategories_seeder::class);
        $this->call(table_products_seeder::class);
    }
}
