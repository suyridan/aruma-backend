<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    * {
    box-sizing: border-box;
    }

    body {
    font-family: Arial, Helvetica, sans-serif;
    }

    /* Float four columns side by side */
    .column {
        float: left;
        width: 33%;
        padding: 0 11px;
    }

    /* Remove extra left and right margins, due to padding */
    .row {
        /* margin: 0 -5px; */
    }

    /* Clear floats after the columns */
    .row:after {
    content: "";
    display: table;
    clear: both;
    }

    /* Responsive columns */
    @media screen and (max-width: 600px) {
        .column {
            width: 100%;
            display: block;
            margin-bottom: 14px;
        }
    }

/* Style the counter cards */
    .card {
        padding: 11px;
        text-align: center;
        background-color: #ffffff;
        height: 250px;
        width: 190px;
    }

    @page {
        margin: 0cm 0cm;
        font-family: Arial;
    }

    body {
        margin: 2.5cm 1cm 1.5cm;
    }

    header {
        position: fixed;
        top: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1.8cm;
        background-color: #2a0927;
        color: white;
        text-align: center;
        line-height: 30px;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1.8cm;
        background-color: #2a0927;
        color: white;
        text-align: center;
        line-height: 30px;
    }

    .img {
        box-shadow: 30px -16px teal;
    }
</style>
</head>
<header>
    <h1>Aruma</h1>
</header>
<body>
<div class="row">
@foreach ($productos as $key => $producto)
        <div class="column">
            <div class="card">
                @if ( $producto->image_portrait )
                    <img class="img" src="{{ url( $producto->image_portrait->url ) }}" width="190" height="190" alt="">
                @else
                    <img class="img" src="{{ url('/img/07_Coral.jpg') }}" width="190" height="190" alt="">
                @endif
                <h6>{{ $producto->name }} <i style="background: yellow; font-size:12px;">Precio: {{ $producto->actual_price->unit_price_purchase }}</i></h6>
            </div>
        </div>
    @if ((($key + 1) % 3) == 0)
</div>
<div class="row">
    @endif

@endforeach
</div>
</body>
<footer>
    <h2>.</h2>
</footer>
</html>
