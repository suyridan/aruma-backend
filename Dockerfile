FROM php:8.0.11-apache

RUN a2enmod rewrite

RUN apt-get update
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y libicu-dev
RUN apt-get install -y libxml2-dev
RUN apt-get install -y libpq-dev
RUN apt-get install -y libzip-dev
RUN apt-get install -y libpng-dev
RUN docker-php-ext-install pdo pdo_pgsql zip intl soap opcache gd

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
COPY docker/php/php.ini /usr/local/etc/php/php.ini
COPY docker/apache/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY docker/apache/apache2.conf /etc/apache2/apache2.conf

ENV COMPOSER_ALLOW_SUPERUSER 1

WORKDIR /var/www/html/
